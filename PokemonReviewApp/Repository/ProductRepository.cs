﻿using DemoBusiness.Interfaces;
using DemoBusinessApp.Constant;
using DemoBusinessApp.Models;
using DemoBusinessApp.ViewModels;
using DemoBusinessApp.ViewModels.Product;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Linq;

namespace DemoBusiness.Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly DemoApiContext _context;
        public ProductRepository(DemoApiContext context)
        {
            _context = context;
        }
        public ResultApi<List<GetProductResponse>> GetProducts(GetProductOverviewRequest request)
        {
            var result = new List<GetProductResponse>();
            try
            {
                IQueryable<GetProductResponse> products = (from a in _context.Products
                                                           join d in _context.ProductUnits on a.ProductUnitId equals d.ProductUnitId
                                                           join c in _context.ProductVats on a.ProductVatId equals c.ProductVatId
                                                           where (string.IsNullOrEmpty(request.ProductNum) || a.ProductNum.Contains(request.ProductNum))
                                                                     && (string.IsNullOrEmpty(request.Name) || a.Name.Contains(request.Name))
                                                                     && (!request.ProductUnitId.HasValue || a.ProductUnitId == request.ProductUnitId)
                                                                     && (!request.ProductVatId.HasValue || a.ProductVatId == request.ProductVatId)
                                                                     && (!request.StartDate.HasValue || a.CreateDate >= request.StartDate.Value)
                                                                     && (!request.EndDate.HasValue || a.CreateDate <= request.EndDate.Value)
                                                           select new GetProductResponse
                                                           {
                                                               ProductVatId = a.ProductVatId,
                                                               ProductNum = a.ProductNum,
                                                               Name = a.Name,
                                                               ProductUnitId = a.ProductUnitId,
                                                               ProductId = a.ProductId,
                                                               BarcodeNum = a.BarcodeNum,
                                                               Description = a.Description,
                                                               HaveBarcode = a.HaveBarcode,
                                                               TypeId = (TypeConstant)a.TypeId,
                                                               TypeName = ((TypeConstant)a.TypeId).ToString(),
                                                               PricePerUnit = a.PricePerUnit,
                                                               ProductUnitName = d.Name,
                                                               ProductVat = c.Value,
                                                               PriceRanges = new List<ProductPriceRange>()
                                                           });

                var productList = products
                                   .AsNoTracking()
                                   .ToDictionary(
                                         grp => grp.ProductId,
                                         grp => grp
                                    );

                var productIds = productList.Keys;

                var priceRanges = _context.PriceRanges
                   .Where(pr => productIds.Contains(pr.ProductId))
                   .Select(pr => new
                   {
                       pr.PriceRangeId,
                       pr.Name,
                       pr.MinQty,
                       pr.MaxQty,
                       pr.Price,
                       pr.ProductId
                   })
                   .AsNoTracking()
                   .ToList();

                foreach (var priceRange in priceRanges)
                {
                    var item = new ProductPriceRange()
                    {
                        PriceRangeId = priceRange.PriceRangeId,
                        Name = priceRange.Name,
                        MinQty = priceRange.MinQty,
                        MaxQty = priceRange.MaxQty,
                        Price = priceRange.Price,
                    };
                    productList[priceRange.ProductId].PriceRanges.Add(item);
                }

                result = productList.Values.ToList();
                return new ResultApi<List<GetProductResponse>> { Message = "Success", StatusCode = 200, Data = result };

            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred while retrieving products: {ex.Message}");
                return new ResultApi<List<GetProductResponse>> { Message = ex.Message, StatusCode = 500 };
            }
        }
        public ResultApi<GetProductResponse> GetProductById(GetProductRequest request)
        {
            try
            {
                var product = (from a in _context.Products
                               join d in _context.ProductUnits on a.ProductUnitId equals d.ProductUnitId
                               join c in _context.ProductVats on a.ProductVatId equals c.ProductVatId
                               where a.ProductId == request.ProductId
                               select new GetProductResponse
                               {
                                   ProductVatId = a.ProductVatId,
                                   ProductNum = a.ProductNum,
                                   Name = a.Name,
                                   ProductUnitId = a.ProductUnitId,
                                   ProductId = a.ProductId,
                                   BarcodeNum = a.BarcodeNum,
                                   Description = a.Description,
                                   HaveBarcode = a.HaveBarcode,
                                   TypeId = (TypeConstant)a.TypeId,
                                   TypeName = ((TypeConstant)a.TypeId).ToString(),
                                   PricePerUnit = a.PricePerUnit,
                                   ProductUnitName = d.Name,
                                   ProductVat = c.Value
                               })
                                .AsNoTracking()
                                .Single();

                var priceRanges = _context.PriceRanges
                                        .Where(pr => pr.ProductId == request.ProductId)
                                        .Select(priceRanges => new ProductPriceRange
                                        {
                                            PriceRangeId = priceRanges.PriceRangeId,
                                            Name = priceRanges.Name,
                                            MinQty = priceRanges.MinQty,
                                            MaxQty = priceRanges.MaxQty,
                                            Price = priceRanges.Price,
                                        })
                                        .AsNoTracking()
                                        .ToList();

                product.PriceRanges = priceRanges;
                return new ResultApi<GetProductResponse> { Message = "Success", StatusCode = 200, Data = product };
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred while retrieving the product with ID {request.ProductId}: {ex.Message}");
                return new ResultApi<GetProductResponse> { Message = ex.Message, StatusCode = 500 };
            }
        }
        public ResultApi<int> AddProduct(CreateOrUpdateRequest request)
        {
            //Check duplicate name
            var requestPriceRanges = request.PriceRanges;
            var result = new Dictionary<string, int>();
            foreach (var item in requestPriceRanges)
            {
                if (result.ContainsKey(item.Name))
                    return new ResultApi<int> { Message = $"Price Range is duplicate name: {item.Name}", StatusCode = 500, Data = 0 };
                else
                    result.Add(item.Name, 0);
            }

            //Check Range Overlab
            int PrevMinPriceRange = 0;
            int PrevMaxPriceRange = 0;
            foreach (var priceRange in requestPriceRanges)
            {
                if (priceRange.MinQty <= PrevMaxPriceRange && PrevMinPriceRange <= priceRange.MaxQty)
                    return new ResultApi<int> { Message = $"Price range is overlab: {priceRange.MinQty} - {priceRange.MaxQty}", StatusCode = 500, Data = 0 };

                PrevMinPriceRange = priceRange.MinQty;
                PrevMaxPriceRange = priceRange.MaxQty;
            }

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //Parent Section
                    Product product = new Product
                    {
                        ProductNum = GenerateProductCode(),
                        Name = request.Name,
                        HaveBarcode = request.HaveBarcode,
                        BarcodeNum = request.BarcodeNum,
                        Description = request.Description,
                        TypeId = (int)request.TypeId,
                        PricePerUnit = request.PricePerUnit,
                        CreateDate = DateTime.Now,
                        ProductUnitId = request.ProductUnitId,
                        ProductVatId = request.ProductVatId,
                    };
                    _context.Add(product);
                    _context.SaveChanges();

                    //Child Section
                    foreach (var priceRange in requestPriceRanges)
                    {
                        var newPriceRange = new PriceRange();
                        newPriceRange.Price = priceRange.Price;
                        newPriceRange.MinQty = priceRange.MinQty;
                        newPriceRange.MaxQty = priceRange.MaxQty;
                        newPriceRange.Name = priceRange.Name;
                        newPriceRange.ProductId = product.ProductId;
                        _context.PriceRanges.Add(newPriceRange);
                    }
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return new ResultApi<int> { Message = "Created", StatusCode = 200, Data = product.ProductId };
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    Console.WriteLine($"An error occurred while adding a new product: {ex.Message}");
                    return new ResultApi<int> { Message = ex.Message, StatusCode = 500, Data = 0 };
                }
            }
        }
        public ResultApi<int> UpdateProduct(CreateOrUpdateRequest request)
        {
            //Check duplicate name
            var requestPriceRanges = request.PriceRanges;
            var result = new Dictionary<string,int>();
            foreach (var item in requestPriceRanges)
            {
                if (result.ContainsKey(item.Name))
                    return new ResultApi<int> { Message = $"Price Range is duplicate name: {item.Name}", StatusCode = 500, Data = 0 };
                else
                    result.Add(item.Name, 0);
            }

            //Check Range Overlab
            int PrevMinPriceRange = 0;
            int PrevMaxPriceRange = 0;
            foreach (var priceRange in requestPriceRanges)
            {
                if (priceRange.MinQty <= PrevMaxPriceRange && PrevMinPriceRange <= priceRange.MaxQty)
                    return new ResultApi<int> { Message = $"Price range is overlab: {priceRange.MinQty} - {priceRange.MaxQty}", StatusCode = 500, Data = 0 };

                PrevMinPriceRange = priceRange.MinQty;
                PrevMaxPriceRange = priceRange.MaxQty;
            }

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    //Update parent
                    var product = _context.Products.Single(p => p.ProductId == request.ProductId);
                    product.TypeId = (int)request.TypeId;
                    product.HaveBarcode = request.HaveBarcode;
                    product.BarcodeNum = request.BarcodeNum;
                    product.Description = request.Description;
                    product.UpdateDate = DateTime.Now;
                    product.Name = request.Name;
                    product.PricePerUnit = request.PricePerUnit;
                    product.ProductUnitId = request.ProductUnitId;
                    product.ProductVatId = request.ProductVatId;

                    //Get Ids for compare and clear data
                    var existingPriceRanges = _context.PriceRanges.Where(pr => pr.ProductId == request.ProductId).ToList();
                    var requestIds = requestPriceRanges.Select(pr => pr.PriceRangeId);

                    foreach (var item in existingPriceRanges)
                    {
                        if (!requestIds.Contains(item.PriceRangeId))
                        {
                            _context.Remove(item);
                        }
                    }

                    //Update Child
                    foreach (var priceRange in requestPriceRanges)
                    {
                        if (priceRange.PriceRangeId.HasValue)
                        {
                            var updatePriceRange = _context.PriceRanges.Single(pr => pr.PriceRangeId == priceRange.PriceRangeId);
                            updatePriceRange.Name = priceRange.Name;
                            updatePriceRange.Price = priceRange.Price;
                            updatePriceRange.MinQty = priceRange.MinQty;
                            updatePriceRange.MaxQty = priceRange.MaxQty;
                        }
                        else
                        {
                            var newPriceRange = new PriceRange();
                            newPriceRange.Price = priceRange.Price;
                            newPriceRange.MinQty = priceRange.MinQty;
                            newPriceRange.MaxQty = priceRange.MaxQty;
                            newPriceRange.Name = priceRange.Name;
                            newPriceRange.ProductId = product.ProductId;
                            _context.PriceRanges.Add(newPriceRange);
                        }
                    }
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return new ResultApi<int> { Message = "Success", StatusCode = 200, Data = request.ProductId };
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    return new ResultApi<int> { Message = ex.Message, StatusCode = 500, Data = 0 };
                }
            }
        }
        public ResultApi<int> DeleteProduct(ProductDeleteRequest request)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var product = _context.Products.Single(p => p.ProductId == request.ProductId);
                    var priceRanges = _context.PriceRanges.Where(pr => pr.ProductId == request.ProductId);

                    _context.RemoveRange(priceRanges);
                    _context.Remove(product);
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return new ResultApi<int> { Message = "Success", StatusCode = 200, Data = request.ProductId };
                }
                catch (Exception ex)
                {
                    dbContextTransaction.Rollback();
                    Console.WriteLine($"An error occurred while deleting the product with ID {request.ProductId}: {ex.Message}");
                    return new ResultApi<int> { Message = ex.Message, StatusCode = 500, Data = 0 };
                }
            }
        }
        string GenerateProductCode()
        {
            var lastProduct = _context.Products.OrderByDescending(p => p.ProductId).FirstOrDefault();

            if (lastProduct != null)
            {
                // หาตัวเลข X ล่าสุดและเพิ่มขึ้น 1
                int lastNumber = int.Parse(lastProduct.ProductNum.Substring(3));
                int nextNumber = lastNumber + 1;

                // สร้างรหัสใหม่
                string nextProductNum = $"PC-{nextNumber:D6}";
                return nextProductNum;
            }
            else
            {
                // ถ้าไม่มีข้อมูลในตาราง Product ให้เริ่มที่ 1
                return "PC-000001";
            }
        }
    }
}
