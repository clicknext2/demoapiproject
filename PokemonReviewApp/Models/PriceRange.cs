﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class PriceRange
    {
        public int PriceRangeId { get; set; }
        public string Name { get; set; } = null!;
        public decimal Price { get; set; }
        public int MinQty { get; set; }
        public int MaxQty { get; set; }
        public int ProductId { get; set; }

        public virtual Product Product { get; set; } = null!;
    }
}
