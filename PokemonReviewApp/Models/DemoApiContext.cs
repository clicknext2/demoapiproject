﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DemoBusinessApp.Models
{
    public partial class DemoApiContext : DbContext
    {
        public DemoApiContext()
        {
        }

        public DemoApiContext(DbContextOptions<DemoApiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<PriceRange> PriceRanges { get; set; } = null!;
        public virtual DbSet<Product> Products { get; set; } = null!;
        public virtual DbSet<ProductUnit> ProductUnits { get; set; } = null!;
        public virtual DbSet<ProductVat> ProductVats { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-B700ROO\\SQLEXPRESS;Database=DemoApi;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PriceRange>(entity =>
            {
                entity.ToTable("PriceRange");

                entity.HasIndex(e => e.PriceRangeId, "IX_PriceRange");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.HasOne(d => d.Product)
                    .WithMany(p => p.PriceRanges)
                    .HasForeignKey(d => d.ProductId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PriceRange_Product");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");

                entity.HasIndex(e => e.ProductId, "Unq_Name")
                    .IsUnique();

                entity.HasIndex(e => e.ProductNum, "Unq_ProductNum")
                    .IsUnique();

                entity.Property(e => e.BarcodeNum).HasMaxLength(50);

                entity.Property(e => e.CreateDate).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.PricePerUnit).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.ProductNum).HasMaxLength(50);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProductUnit)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ProductUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductUnit1");

                entity.HasOne(d => d.ProductVat)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ProductVatId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Product_ProductVat");
            });

            modelBuilder.Entity<ProductUnit>(entity =>
            {
                entity.ToTable("ProductUnit");

                entity.Property(e => e.ProductUnitId).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(500);
            });

            modelBuilder.Entity<ProductVat>(entity =>
            {
                entity.ToTable("ProductVat");

                entity.Property(e => e.ProductVatId).ValueGeneratedNever();

                entity.Property(e => e.Value).HasColumnType("decimal(18, 2)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
