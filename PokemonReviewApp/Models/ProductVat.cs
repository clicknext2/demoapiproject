﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class ProductVat
    {
        public ProductVat()
        {
            Products = new HashSet<Product>();
        }

        public int ProductVatId { get; set; }
        public decimal Value { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
