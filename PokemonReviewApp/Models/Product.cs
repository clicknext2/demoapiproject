﻿using System;
using System.Collections.Generic;

namespace DemoBusinessApp.Models
{
    public partial class Product
    {
        public Product()
        {
            PriceRanges = new HashSet<PriceRange>();
        }

        public int ProductId { get; set; }
        public string ProductNum { get; set; } = null!;
        public int TypeId { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public bool HaveBarcode { get; set; }
        public string? BarcodeNum { get; set; }
        public decimal PricePerUnit { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int ProductVatId { get; set; }
        public int ProductUnitId { get; set; }

        public virtual ProductUnit ProductUnit { get; set; } = null!;
        public virtual ProductVat ProductVat { get; set; } = null!;
        public virtual ICollection<PriceRange> PriceRanges { get; set; }
    }
}
