﻿using DemoBusinessApp.ViewModels;
using DemoBusinessApp.ViewModels.Product;

namespace DemoBusiness.Interfaces
{
    public interface IProductRepository
    {
        ResultApi<List<GetProductResponse>> GetProducts(GetProductOverviewRequest request);
        ResultApi<GetProductResponse> GetProductById(GetProductRequest request);
        ResultApi<int> AddProduct(CreateOrUpdateRequest request);
        ResultApi<int> UpdateProduct(CreateOrUpdateRequest request);
        ResultApi<int> DeleteProduct(ProductDeleteRequest request);
    }
}
