﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DemoBusinessApp.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductUnit",
                columns: table => new
                {
                    ProductUnitId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductUnit", x => x.ProductUnitId);
                });

            migrationBuilder.CreateTable(
                name: "ProductVat",
                columns: table => new
                {
                    ProductVatId = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductVat", x => x.ProductVatId);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    ProductId = table.Column<int>(type: "int", nullable: false),
                    Type = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Info = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    HaveBarcode = table.Column<bool>(type: "bit", nullable: true),
                    ProductVatId = table.Column<int>(type: "int", nullable: true),
                    ProductUnitId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_Product_ProductUnit1",
                        column: x => x.ProductUnitId,
                        principalTable: "ProductUnit",
                        principalColumn: "ProductUnitId");
                    table.ForeignKey(
                        name: "FK_Product_ProductVat",
                        column: x => x.ProductVatId,
                        principalTable: "ProductVat",
                        principalColumn: "ProductVatId");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Product_ProductUnitId",
                table: "Product",
                column: "ProductUnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_ProductVatId",
                table: "Product",
                column: "ProductVatId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "ProductUnit");

            migrationBuilder.DropTable(
                name: "ProductVat");
        }
    }
}
