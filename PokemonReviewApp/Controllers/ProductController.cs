﻿using DemoBusiness.Interfaces;
using DemoBusinessApp.ViewModels.Product;
using Microsoft.AspNetCore.Mvc;

namespace DemoBusiness.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;
        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [HttpGet]
        public IActionResult GetProducts([FromQuery] GetProductOverviewRequest request)
        {   
            var result = _productRepository.GetProducts(request); 
            return Ok(result);
        }

        [HttpGet]
        public IActionResult GetProductById([FromQuery] GetProductRequest request)
        {
            var result = _productRepository.GetProductById(request);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult AddProduct([FromBody] CreateOrUpdateRequest request)
        {
            var result = _productRepository.AddProduct(request);
            return Ok(result);
        }

        [HttpPost]
        public IActionResult UpdateProduct([FromBody] CreateOrUpdateRequest request)
        {
            var result = _productRepository.UpdateProduct(request);
            return Ok(result);
        }

        [HttpPost] 
        public IActionResult DeleteProduct([FromBody] ProductDeleteRequest request)
        {
            var result = _productRepository.DeleteProduct(request);
            return Ok(result);
        }
    }
}
