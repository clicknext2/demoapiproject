﻿using DemoBusinessApp.Constant;
using System.ComponentModel.DataAnnotations;

namespace DemoBusinessApp.ViewModels.Product
{
    public class CreateOrUpdateRequest : IValidatableObject
    {
        public int ProductId { get; set; }
        public string ProductNum { get; set; } = "";
        public TypeConstant TypeId { get; set; }
        public string? TypeName { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public bool HaveBarcode { get; set; }
        public string? BarcodeNum { get; set; }
        public decimal PricePerUnit { get; set; }
        public int ProductVatId { get; set; }
        public int ProductUnitId { get; set; }
        public List<ProductPriceRange> PriceRanges { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (HaveBarcode && (BarcodeNum == null || string.IsNullOrEmpty(BarcodeNum.ToString())))
                yield return new ValidationResult("BarcodeNum is required when HaveBarcode is true.", new[] { nameof(BarcodeNum) });
            if (!HaveBarcode && BarcodeNum != null)
                yield return new ValidationResult("BarcodeNum should be null when HaveBarcode is false.", new[] { nameof(BarcodeNum) });
        }
    }
}
