﻿using DemoBusinessApp.Constant;
using System.ComponentModel.DataAnnotations;

namespace DemoBusinessApp.ViewModels.Product
{
    public class GetProductRequest
    {
        public int ProductId { get; set; }
    }
    public class GetProductResponse
    {
        public int ProductId { get; set; }
        public string ProductNum { get; set; }
        public TypeConstant TypeId { get; set; }
        public string? TypeName { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public bool HaveBarcode { get; set; }
        public string? BarcodeNum { get; set; }
        public decimal PricePerUnit { get; set; }
        public int ProductVatId { get; set; }
        public int ProductUnitId { get; set; }
        public string ProductUnitName { get; set; }
        public decimal ProductVat { get; set; }
        public List<ProductPriceRange> PriceRanges { get; set; }
    }

    public class PriceRangeResult
    {
        public int PriceRangeId { get; set; }
        public string Name { get; set; } = null!;
        public decimal Price { get; set; }
        public int MinQty { get; set; }
        public int MaxQty { get; set; }
        public int ProductId { get; set; }
    }

    public class ProductPriceRange : IValidatableObject
    {
        public int? PriceRangeId { get; set; }
        public string Name { get; set; } = null!;
        public decimal Price { get; set; }
        public int MinQty { get; set; }
        public int MaxQty { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (MinQty <= 0)
            {
                yield return new ValidationResult("MinQty must be greater than 0.", new[] { nameof(MinQty) });
            }

            if (MaxQty <= 0)
            {
                yield return new ValidationResult("MaxQty must be greater than 0.", new[] { nameof(MaxQty) });
            }

            if (MaxQty <= MinQty)
            {
                yield return new ValidationResult("MaxQty must be greater than or equal to MinQty.", new[] { nameof(MaxQty), nameof(MinQty) });
            }
        }

    }
}
