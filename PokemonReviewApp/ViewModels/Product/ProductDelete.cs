﻿namespace DemoBusinessApp.ViewModels.Product
{
    public class ProductDeleteRequest
    {
        public int ProductId { get; set; }
    }
}
