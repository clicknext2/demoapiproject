﻿namespace DemoBusinessApp.ViewModels.Product
{
    public class GetProductOverviewRequest
    {
        public string? ProductNum { get; set; }
        public string? Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? ProductVatId { get; set; }
        public int? ProductUnitId { get; set; }
    }
}
